﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Web;
using System.Net.Mail;
using System.Net.Security;
using System.Net;


namespace email
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
         
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // adres maila do wysłania i nazwa obrazka
            string mail = "platf.progr16@gmail.com";
           // string obrazek = "zdj.jpg";

            // szczegóły wiadomości: odbiorca, nadawca, temat, treść wiadomośći
            var message = new MailMessage();
            message.From = new MailAddress("platf.progr16@gmail.com");
            message.To.Add(new MailAddress(mail));
            message.Subject = "Temat maila";
            message.Body = "Treść maila";

            // protokół smtp
            var smtp = new SmtpClient("smtp.gmail.com", 587);
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential("platf.progr16@gmail.com", "student16");
            smtp.EnableSsl = true;


            // pobieranie obrazka
            WebClient webClient = new WebClient();
            webClient.DownloadFile("http://img7.demotywatoryfb.pl//uploads/201603/1457830001_uogmfi_600.jpg", "zdj.jpg");
            //webClient.DownloadFileAsync(new Uri("http://img7.demotywatoryfb.pl//uploads/201603/1457830001_uogmfi_600.jpg"), "zdj");
 

            // załączenie obrazka i wysłanie wiadomości
            Attachment attach = new Attachment("zdj.jpg");
            message.Attachments.Add(attach);
            smtp.Send(message);

            message.Attachments.Clear();
            
        }
    }
}
